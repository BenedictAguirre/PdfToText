package com.svi.pdftotext.dataprocessor;

import java.io.File;

import com.svi.pdftotext.config.Config;
import com.svi.pdftotext.pdfparser.PdfReader;
import com.svi.pdftotext.textwriter.TextWriter;

public class PdfToTextWriter {
	private static final String PDF_FILE_PATH = Config.getProperty("pdf_file_path");
	private static final String TEXT_FOLDER_PATH = Config.getProperty("text_folder_path");
	PdfReader pdfReader = new PdfReader();
	TextWriter textWriter = new TextWriter();
	public static String getFileNameWithoutExtension(File file) {
	    String fileName = null;
	    if (file.getName().startsWith(".")) {
	        fileName = file.getName();
	    } else {
	        try {
	            fileName = file.getName().substring(0, file.getName().lastIndexOf("."));
	        } catch (Exception e) {
	            fileName = file.getName();
	        }
	    }

	    return fileName;
	}
	public String getTextFilePath() {
		String name = null;
		File pdfFile = new File(PDF_FILE_PATH);
		String fileName = getFileNameWithoutExtension(pdfFile);
		name = TEXT_FOLDER_PATH+"/" + fileName + ".txt";
		return name;
	}
	public void writePdfToTextFile() {
		String message = pdfReader.getPdfTextContent(PDF_FILE_PATH);
		textWriter.writeToTextFile(getTextFilePath(), message);
	}

	public static void main(String[] args) {
		PdfToTextWriter writer = new PdfToTextWriter();
		writer.writePdfToTextFile();
	}

}
