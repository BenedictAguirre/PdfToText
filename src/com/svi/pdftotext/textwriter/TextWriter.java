package com.svi.pdftotext.textwriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextWriter {
	public void writeToTextFile(String textFilePath, String message) {
		File file = new File(textFilePath); // initialize File object and passing path as argument
		boolean result;
		try {
			result = file.createNewFile();
			if (result)
			{
				System.out.println("file created " + file.getCanonicalPath()); 
			} else {
				System.out.println("File already exist at location: " + file.getCanonicalPath());
			}
			FileWriter writer = new FileWriter(file);
	            writer.write(message);
	            writer.close();
		} catch (IOException e) {
			e.printStackTrace(); // prints exception if any
		}
	}
}
